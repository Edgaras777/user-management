<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupsRepository")
 */
class Groups
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Customer", mappedBy="groups")
     */
    private $group_name;

    public function __construct()
    {
        $this->group_name = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getGroupName(): Collection
    {
        return $this->group_name;
    }

    public function addGroupName(Customer $groupName): self
    {
        if (!$this->group_name->contains($groupName)) {
            $this->group_name[] = $groupName;
            $groupName->setGroups($this);
        }

        return $this;
    }

    public function removeGroupName(Customer $groupName): self
    {
        if ($this->group_name->contains($groupName)) {
            $this->group_name->removeElement($groupName);
            // set the owning side to null (unless already changed)
            if ($groupName->getGroups() === $this) {
                $groupName->setGroups(null);
            }
        }

        return $this;
    }
}
