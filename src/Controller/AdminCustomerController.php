<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Groups;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminCustomerController extends AbstractController
{
    /**
     * @Route("/admin/list", name="admin_list")
     */
    public function list(CustomerRepository $customerRepository)
    {
        return $this->render('admin/list.html.twig', [
            'customers' => $customerRepository->getCustomersWithGroups()
        ]);
    }

    /**
    * @Route("/admin/create-customer", name="admin_create_customer")
    */
    public function createCustomer(Request $request): Response
    {
        $customer = new Customer();

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('new', SubmitType::class, ['label' => 'Create User'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $name = $form->get('name')->getData();
            $customer->setName($name);
            $em->persist($customer);
            $em->flush();
            return $this->render('admin/create.html.twig', [
                'form' => $form->createView(),
                'success' => $name
            ]);
        }

        return $this->render('admin/create.html.twig', [
            'form' => $form->createView(),
            'success' => isset($name) ? $name : ''
        ]);
    }

    /**
     * @Route("/admin/customer-delete", name="admin_customer_delete")
     */
    public function delete(Request $request)
    {
        $id = $request->query->get('id');
        $customer = $this->getDoctrine()->getRepository(Customer::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($customer);
        $em->flush();

        return $this->redirectToRoute('admin_list');
    }

    /**
     * @Route("/admin/customer-assign", name="admin_customer_assign")
     */
    public function customerAssignToGroup(Request $request): Response
    {
        $id = $request->query->get('id');
        $customer = $this->getDoctrine()->getRepository(Customer::class)->find($id);

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('groups', EntityType::class,[
                'class' => Groups::class,
                'choice_label' => 'name'
            ])
            ->add('submit', SubmitType::class, ['label' => 'Assign customer'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $name = $form->get('name')->getData();
            $em->persist($customer);
            $em->flush();
        }

        return $this->render('admin/customer-assign.html.twig', [
            'form' => $form->createView(),
            'success' => isset($name) ? $name : ''
        ]);

    }

}
