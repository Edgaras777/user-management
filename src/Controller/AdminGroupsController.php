<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Groups;
use App\Repository\CustomerRepository;
use App\Repository\GroupsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminGroupsController extends AbstractController
{
    /**
     * @Route("/admin/groups", name="admin_groups")
     */
    public function index()
    {
        return $this->render('admin_groups/index.html.twig', [
            'controller_name' => 'AdminGroupsController',
        ]);
    }

    /**
     * @Route("/admin/create-group", name="admin_create_group")
     */
    public function createGroup(Request $request): Response
    {
        $groups = new Groups();

        $form = $this->createFormBuilder($groups)
            ->add('name', TextType::class)
            ->add('new', SubmitType::class, ['label' => 'Create group'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $name = $form->get('name')->getData();
            $groups->setName($name);
            $em->persist($groups);
            $em->flush();
            return $this->render('admin/create-group.html.twig', [
                'form' => $form->createView(),
                'success' => $name
            ]);
        }

        return $this->render('admin/create-group.html.twig', [
            'form' => $form->createView(),
            'success' => isset($name) ? $name : ''
        ]);
    }

    /**
     * @Route("/admin/group/list", name="admin_group_list")
     */
    public function groupList(GroupsRepository $groupsRepository)
    {
        return $this->render('admin/group-list.html.twig', [
            'groups' => $groupsRepository->getAllGroupsList()
        ]);
    }

    /**
     * @Route("/admin/group/delete", name="admin_group_delete")
     */
    public function delete(Request $request, CustomerRepository $customerRepository)
    {
        $id = $request->query->get('id');
        $em = $this->getDoctrine()->getManager();
        $groups = $this->getDoctrine()->getRepository(Groups::class)->find($id);
        $haveGroups = $customerRepository->findCustomersWithGroups($id);
        if (isset($haveGroups) && !empty($haveGroups)) {
            return $this->redirectToRoute('admin_group_list', array('status' => 'unsuccessful'));
        } else {
            $em->remove($groups);
            $em->flush();
            return $this->redirectToRoute('admin_group_list', array('status' => 'successful'));
        }
    }
}
