<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CustomerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $names = ['Simon','Jon','Dave','Danis'];

        for ($i = 0 ; $i < 4; $i++){
            $customer = new Customer();
            $customer->setName(sprintf($names[$i]));
            $manager->persist($customer);
        }

        $manager->flush();
    }
}
