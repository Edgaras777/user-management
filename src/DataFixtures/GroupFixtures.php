<?php

namespace App\DataFixtures;

use App\Entity\Groups;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class GroupFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $groups = ['Customer','Seller','Administrator','Manager'];

        for ($i = 0 ; $i < 4; $i++){
            $group = new Groups();
            $group->setName(sprintf($groups[$i]));
            $manager->persist($group);
        }

        $manager->flush();
    }
}
