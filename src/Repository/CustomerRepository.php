<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\Groups;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    /**
    * @return Customer[] Returns an array of Customer objects
    */
    public function getCustomerList()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id > 0')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Customer[] Returns an array of Customer objects with Groups relation
     */
    public function getCustomersWithGroups()
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.groups', 'g')
            ->addSelect('g')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult()
         ;
    }

    /**
     * @return Customer[] Returns an array of Customer object
     */
    public function findCustomer($id)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult()
        ;

    }

    /**
     * @return Customer[] Returns an array of Customers with groups
     */
    public function findCustomersWithGroups($id)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.groups = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult()
            ;

    }
}
