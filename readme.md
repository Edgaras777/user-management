# User management
### Installation
```sh
$ cd project-folder
$ composer install && update
$ php bin/console doctrine:schema:update
$ php bin/console doctrine:fixtures:load
$ php bin/console server:run
$ Should be good to go 127.0.0.1:8080/login
$ Login with seeded user: admin, password: root
```
